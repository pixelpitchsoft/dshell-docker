# README #

Docker image for network forensic analysis framework [Dshell](https://github.com/USArmyResearchLab/Dshell)

### Dshell Docker container ###

* Easy launching Dshell as Docker container
* v0.0.2

### Setting up ###

* Setting up:
```shell
docker run -i -t veritus4/dshell-docker
```
* No additional configuration required
* Dependencies: [Docker](https://www.docker.com/)

### Contribution guidelines ###

* Testing with various network cards
* Review code adhering to coding standards

### Who do I talk to? ###

* Admin: [Povilas B.](https://lt.linkedin.com/in/povilasbrilius)
* Other team member contact: [Medardas M.](https://www.linkedin.com/in/medardas-mikolajunas-b9939986)